var todoController = require('./controllers/todoController');

(function(todo){
    var ioSocket = null;
    var routing = {
        getRoutes: function(app, io){
            ioSocket = io;

            app.get('/service/v1/todos', todo.find);
            app.post('/service/v1/todos', function(req, res){
                todo.add(req, res, ioSocket);
            });
            app.delete('/service/v1/todos/:id', function(req, res){
                todo.remove(req, res, ioSocket);
            });
            app.patch('/service/v1/todos/:id', function(req, res){
                todo.modify(req, res, ioSocket);
            });
        }
    };
    exports.getRoutes = routing.getRoutes;
})(todoController)