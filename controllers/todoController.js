var Todo = require('../models/TodoModel');

(function(){

   var actions = {

       /**
        * Recuperation de toutes les todos
        * @param req
        * @param res
        */
        find: function(req, res){
           Todo.model.find({}, function(error, data){
               res.send({status: 'ok', todo: data});
           });
        },

       /**
        * Ajout d'une item dans la liste
        * @param req
        * @param res
        */
       add: function(req, res, socketIO){
           Todo.model.addTodo(req.body.title, function(error, data){
               var responseObject = {
                   status: 'ok',
                   todo: data
               };

               socketIO.emit('addTodo', responseObject);
               res.send(responseObject);
           });
       },

       /**
        * Supprime une item dans la liste
        * @param req
        * @param res
        */
       remove: function(req, res, socketIO){
           Todo.model.removeTodo(req.params.id, function(error, data){
               var responseObject = {
                   status: 'ok',
                   id: req.params.id
               };

               socketIO.emit('removeTodo', responseObject);
               res.send(responseObject);
           });
       },

       /**
        * Modifie une item de la liste
        * @param req
        * @param res
        */
       modify: function(req, res, socketIO){
           if(req.body.done !== undefined ){
               Todo.model.modifyTodo(req.params.id, {done: req.body.done}, function(error, data){
                   var responseObject = {
                       status: 'ok',
                       todo: data
                   };
                   socketIO.emit('modifyTodo', responseObject);
                   res.send(responseObject);
               });
           }
           else{
               res.send({status: 'ko'});
           }
       }
   };

    module.exports = actions;
})()