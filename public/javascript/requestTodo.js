'use strict';

app.factory('requestTodo',['$rootScope', '$http',
    function ($rootScope, $http) {
        var url = '/service/v1/todos/';

        return {
            find: function(callback){
                $http.get(url, {}).
                    success(function(response, status, headers, config) {
                        if(response.status === 'ok' && callback !== undefined){
                            callback(response);
                        }
                    }).
                    error(function(response, status, headers, config) {
                        console.log('Post error from rest service');
                    });
            },

            add: function (data, callback) {
                $http.post(url, data).
                    success(function(response, status, headers, config) {
                        if(response.status === 'ok' && callback !== undefined){
                            callback(response);
                        }
                    }).
                    error(function(response, status, headers, config) {
                        console.log('Post error from rest service');
                    });
            },

            remove: function (id, callback) {
                $http.delete(url+id, {}).
                    success(function(response, status, headers, config) {
                        if(response.status === 'ok' && callback !== undefined){
                            callback(response);
                        }
                    }).
                    error(function(response, status, headers, config) {
                        console.log('Delete error from rest service');
                    });
            },

            modify: function (id, data, callback) {
                $http.patch(url+id, data).
                    success(function(response, status, headers, config) {
                        if(response.status === 'ok' && callback !== undefined){
                            callback(response);
                        }
                    }).
                    error(function(data, status, headers, config) {
                        console.log('Patch error from rest service');
                    });
            }
        };
}]);