app.controller('TodoController', ['$scope', 'requestTodo', 'socketIO', function($scope, requestTodo, socketIO) {
        $scope.todos;

        // Get all Todo list
        requestTodo.find(function(data){
            $scope.todos = data.todo;
        });

        // Add a todo to the list
        $scope.addTodo = function(){
            requestTodo.add({title: $scope.todoText});
        }

        // Remove a todo from the list
        $scope.removeTodo = function(id){
            requestTodo.remove(id, remove);
        };

        // Check a todo as done
        $scope.doneToto = function(id, done){
            requestTodo.modify(id, {done: done});
        }

        $scope.remaining = function() {
            var count = 0;
            angular.forEach($scope.todos, function(todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };

        $scope.archive = function() {
            var oldTodos = $scope.todos;
            $scope.todos = [];
            angular.forEach(oldTodos, function(todo) {
                if (!todo.done) $scope.todos.push(todo);
            });
        };



        // Add a todo in the interface
        function add(data){
            $scope.todos.push(data.todo);
            $scope.todoText = '';
        }

        // Remove a todo in the interface
        function remove(data){
            angular.forEach($scope.todos, function(todo, key) {
                if(todo._id === data.id){
                    $scope.todos.splice(key, 1);
                }
            });
        }

        // Modify a todo in the interface
        function done(data){
            angular.forEach($scope.todos, function(todo, key) {
                if(todo._id === data.todo._id){
                    $scope.todos[key].done = data.todo.done;
                }
            });
        }


        // Socket Management
        socketIO.on('addTodo', add);

        socketIO.on('removeTodo', remove);

        socketIO.on('modifyTodo', done);

    }]);